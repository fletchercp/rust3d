use super::{color::Color, model::Model, renderer::Renderer};

pub struct Node {
    pub name: String,
    pub children: Vec<Node>,
    pub model: Option<Box<dyn Model>>,
    pub renderer: Option<Box<dyn Renderer>>,
    pub color: Option<Box<dyn Color>>,
}
