use super::node::Node;

pub struct SceneGraph {
    pub root: Option<Node>,
}

impl SceneGraph {
    pub fn new() -> SceneGraph {
        SceneGraph { root: None }
    }

    pub fn with_root(mut self, root: Node) -> SceneGraph {
        self.root = Some(root);
        self
    }
}
